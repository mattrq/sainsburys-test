<?php

namespace spec\GMR\Scraper\Parser;

use GMR\Scraper\Parser\Product;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class ProductSpec
 *
 * @package spec\GMR\Scraper\Parser
 */
class ProductSpec extends ObjectBehavior
{

    /**
     *
     */
    function it_is_initializable()
    {
        $this->shouldHaveType(Product::class);
    }

    /**
     *
     */
    function it_is_parseable()
    {
        $content = <<<EOF
<div class="pdp">
    
	     
    <div class="productSummary">
        <div class="productTitleDescriptionContainer">
            <h1>Sainsbury's Apricot Ripe & Ready x5</h1>
	 
            <div id="productImageHolder">
	             <img src="http://www.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/product_images/media_7572754_L.jpg"  alt="Image for Sainsbury&#039;s Apricot Ripe &amp; Ready x5 from Sainsbury&#039;s" class="productImage " id="productImageID" />
            </div>  
	
            
	
            <div class="reviews">   
				<!-- BEGIN CatalogEntryRatingsReviewsInfoDetailsPage.jspf --><!-- END CatalogEntryRatingsReviewsInfoDetailsPage.jspf -->
            </div>
        </div>
        
        
	   <div class="addToTrolleytabBox" >
	        <!-- Start UserSubscribedOrNot.jspf --><!-- Start UserSubscribedOrNot.jsp --><!-- 
			If the user is not logged in, render this opening 
			DIV adding an addtional class to fix the border top which is removed 
			and replaced by the tabs
		-->
		<div class="addToTrolleytabContainer addItemBorderTop">
	<!-- End AddToSubscriptionList.jsp --><!-- End AddSubscriptionList.jspf --><!-- 
		    ATTENTION!!!
		    <div class="addToTrolleytabContainer">
		    This opening div is inside "../../ReusableObjects/UserSubscribedOrNot.jsp"
		    -->
	        <div class="pricingAndTrolleyOptions">
	        
	        
	        <div class="priceTab activeContainer priceTabContainer" id="addItem_149117"> <!-- CachedProductOnlyDisplay.jsp -->
	            <div class="pricing">
	               
<p class="pricePerUnit">
&pound;3.50<abbr title="per">/</abbr><abbr title="unit"><span class="pricePerUnitUnit">unit</span></abbr>
</p>
 
    <p class="pricePerMeasure">&pound;0.70<abbr 
            title="per">/</abbr><abbr 
            title="each"><span class="pricePerMeasureMeasure">ea</span></abbr>
    </p>

	            </div>
	            
	            <div class="addToTrolleyForm "> 
	                
<form class="addToTrolleyForm" name="OrderItemAddForm_149117" action="OrderItemAdd" method="post" id="OrderItemAddForm_149117" class="addToTrolleyForm">
    <input type="hidden" name="storeId" value="10151"/>
    <input type="hidden" name="langId" value="44"/>
    <input type="hidden" name="catalogId" value="10122"/>
    <input type="hidden" name="URL" value="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet//ProductDisplay?catalogId=10122&amp;level=2&amp;errorViewName=ProductDisplayErrorView&amp;langId=44&amp;categoryId=185749&amp;productId=149116&amp;storeId=10151"/>
    <input type="hidden" name="errorViewName" value="ProductDisplayView"/>
    <input type="hidden" name="SKU_ID" value="7572754"/>
    
        <label class="access" for="quantity_149116">Quantity</label>
        
	        <input name="quantity" id="quantity_149116" type="text" size="3" value="1" class="quantity"   />
	        
        
        <input type="hidden" name="catEntryId" value="149117"/>
        <input type="hidden" name="productId" value="149116"/>
    
    <input type="hidden" name="page" value=""/>
    <input type="hidden" name="contractId" value=""/>
    <input type="hidden" name="calculateOrder" value="1"/>
    <input type="hidden" name="calculationUsage" value="-1,-2,-3"/>
    <input type="hidden" name="updateable" value="1"/>
    <input type="hidden" name="merge" value="***"/>
     
   	<input type="hidden" name="callAjax" value="false"/>
    
         <input class="button process" type="submit" name="Add" value="Add" />
     
</form>

	    <div class="numberInTrolley hidden numberInTrolley_149117" id="numberInTrolley_149117">
	        
	    </div>
	      
	            </div> 
	            
	        </div>            
	        <!-- Start AddToSubscriptionList.jspf --><!-- Start AddToSubscriptionList.jsp --><!-- End AddToSubscriptionList.jsp --><!-- End AddToSubscriptionList.jspf -->     
            </div><!-- End pricingAndTrolleyOptions -->  
        </div><!-- End addToTrolleytabContainer --> 
    </div>	
	
	<div class="BadgesContainer">    
	    
		<div class="roundelContainer">
		          
		 </div>
	    
    </div>
    	 
    
    <div id="sitecatalyst_ESPOT_NAME_WF_013_eSpot_1" class="siteCatalystTag">WF_013_eSpot_1</div>

</div> 

	
        
</div>
<div class="mainProductInfoWrapper">
    <div class="mainProductInfo">        
        <p class="itemCode">
            Item code: 7572754
        </p>
        
        <div class="socialLinks">
        <h2 class="access">Social Links (may open in a new window)</h2>
        
           <ul>
                                   
               <li class="twitter"><a href="https://twitter.com/share?text=Check this out&amp;url=http://www.sainsburys.co.uk/shop/gb/groceries/sainsburys-apricot-ripe---ready-320g" target="_blank"><span>Tweet</span> <span class="access">on Twitter</span></a></li>
                                  
                   <li class="facebook">
                       <iframe src="//www.facebook.com/plugins/like.php?href=http://www.sainsburys.co.uk/shop/gb/groceries/sainsburys-apricot-ripe---ready-320g&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                   </li>
               
           </ul>
        </div>
        
           
        <div class="tabs">
            
            <ul class="tabLinks">
                <li class="first">
                    <a href="#information" class="currentTab">Information</a>
                </li>
                
            </ul>

            
            
            <div class="section" id="information">
                <h2 class="access">Information</h2>
                <ProductContent xmlns:a="http://www.inspire-js.com/SOL">
<HTMLContent contentPath="/Content/media/html/products/label//_label_inspire.html" outputMethod="xhtml">
<h3 class="productDataItemHeader">Description</h3>
<div class="productText">
<p>Apricots</p>
<p>
<p></p>
</p>
</div>

<h3 class="productDataItemHeader">Nutrition</h3>
<div class="productText">
<div>
<p>
<strong>Table of Nutritional Information</strong>
</p>
<div class="tableWrapper">
<table class="nutritionTable">
<thead>
<tr class="tableTitleRow">
<th scope="col">Typical Values as Consumed</th><th scope="col">Per 100g&nbsp;</th><th scope="col">% based on RI for Average Adult</th>
</tr>
</thead>
<tr class="tableRow1">
<th scope="row" class="rowHeader" rowspan="2">Energy</th><td class="tableRow1">155kJ</td><td class="tableRow1">-</td>
</tr>
<tr class="tableRow0">
<td class="tableRow0">37kcal</td><td class="tableRow0">2%</td>
</tr>
<tr class="tableRow1">
<th scope="row" class="rowHeader">Fat</th><td class="tableRow1">&lt;0.5g</td><td class="tableRow1">-</td>
</tr>
<tr class="tableRow0">
<th scope="row" class="rowHeader">Saturates</th><td class="tableRow0">&lt;0.1g</td><td class="tableRow0">-</td>
</tr>
<tr class="tableRow1">
<th scope="row" class="rowHeader">Carbohydrate</th><td class="tableRow1">7.2g</td><td class="tableRow1">3%</td>
</tr>
<tr class="tableRow0">
<th scope="row" class="rowHeader">Total Sugars</th><td class="tableRow0">7.2g</td><td class="tableRow0">8%</td>
</tr>
<tr class="tableRow1">
<th scope="row" class="rowHeader">Fibre</th><td class="tableRow1">1.7g</td><td class="tableRow1">-</td>
</tr>
<tr class="tableRow0">
<th scope="row" class="rowHeader">Protein</th><td class="tableRow0">0.9g</td><td class="tableRow0">2%</td>
</tr>
<tr class="tableRow1">
<th scope="row" class="rowHeader">Salt</th><td class="tableRow1">&lt;0.01g</td><td class="tableRow1">-</td>
</tr>
</table>
</div>
<p>RI= Reference Intakes of an average adult (8400kJ / 2000kcal)</p>
</div>
</div>

<h3 class="productDataItemHeader">Size</h3>
<div class="productText">
<p>5Count</p>
</div>

<h3 class="productDataItemHeader">Packaging</h3>
<div class="productText">
<p>Corrugated board box</p>
<p>RPET / PET punnet</p>
<p>Plastic - RPET film</p>
<p>Paper laminate label -glued</p>
</div>

<h3 class="productDataItemHeader">Manufacturer</h3>
<div class="productText">
<p>We are happy to replace this item if it is not satisfactory</p>
<p>Sainsbury's Supermarkets Ltd.</p>
<p>33 Holborn, London EC1N 2HT</p>
<p>Customer services 0800 636262</p>
</div>

</HTMLContent>
</ProductContent>

                <p><h3>Important Information</h3><p>The above details have been prepared to help you select suitable products. Products and their ingredients are liable to change.</p><p><strong>You should always read the label before consuming or using the product and never rely solely on the information presented here.</p></strong><p>If you require specific advice on any Sainsbury's branded product, please contact our Customer Careline on 0800 636262. For all other products, please contact the manufacturer.</p><p>
This information is supplied for your personal use only. It may not be reproduced in any way without the prior consent of Sainsbury's Supermarkets Ltd and due acknowledgement.</p></p>
            </div>

               
        </div>    
        
            <p class="skuCode">7572754</p>
        
    </div>        
</div>
<div id="additionalItems_149117" class="additionalProductInfo">

      <!--  Left hand side column --><!-- BEGIN MerchandisingAssociationsDisplay.jsp --><!-- Start - JSP File Name:  MerchandisingAssociationsDisplay.jsp --><!-- END MerchandisingAssociationsDisplay.jsp -->
    
    <div class="badges">
        <ul>
            
                    
                    
                    
                     <li >

                        
                        
                        <img src="http://www.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/icons/ico_spacer.gif" alt="Vegetarian" />
                            
                        
                    </li>
                
                    
                    
                    
                     <li  class="lastchild" >

                        
                        
                        <img src="http://www.sainsburys.co.uk/wcsstore7.11.1.161/SainsburysStorefrontAssetStore/wcassets/icons/ico_spacer.gif" alt="Vegan" />
                            
                        
                    </li>
                
        </ul>   
    </div>


  </div>    
    
<!-- END CachedProductOnlyDisplay.jsp -->
          </div><!-- productContent End -->
      </div>
      <!-- Content End --><!-- auxiliary Start -->
      <div class="aside" id="auxiliary">
        <!-- BEGIN RightHandSide.jspf -->
<div id="auxiliaryDock">
    <!-- BEGIN RightHandSide.jsp -->

<div class="panel loginPanel">
	
    <div id="sitecatalyst_ESPOT_NAME_NZ_Welcome_Back_RHS_Espot" class="siteCatalystTag">NZ_Welcome_Back_RHS_Espot</div>

                    
	<h2>Already a customer?</h2>
    <form name="signIn" method="post" action="LogonView" id="Rhs_signIn">
        <input type="hidden" name="storeId" value="10151"/>
        <input type="hidden" name="langId" value="44"/>
        <input type="hidden" name="catalogId" value="10122"/>
        <input type="hidden" name="URL" value="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet//ProductDisplay?catalogId=10122&level=2&errorViewName=ProductDisplayErrorView&langId=44&categoryId=185749&productInRange=true&productId=149116&storeId=10151"/>
        <input type="hidden" name="logonCallerId" value="LogonButton"/>
        <input type="hidden" name="errorViewName" value="ProductDisplayView"/>
        <input class="button process" type="submit" value="Log in" />
    </form>
    
	<div class="panelFooter">
		<p class="register">Not yet registered? 
		<a class="callToAction" name="register" href="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/PostcodeCheckView?catalogId=&currentPageUrl=http%3A%2F%2Fwww.sainsburys.co.uk%2Fwebapp%2Fwcs%2Fstores%2Fservlet%2F%2FProductDisplay%3FcatalogId%3D10122%26level%3D2%26errorViewName%3DProductDisplayErrorView%26langId%3D44%26categoryId%3D185749%26productInRange%3Dtrue%26productId%3D149116%26storeId%3D10151&langId=44&storeId=10151"> Register Now</a></p>
	</div>
</div>
<div class="panel imagePanel checkPostCodePanel" id="checkPostCodePanel">
	
    <div id="sitecatalyst_ESPOT_NAME_NZ_Do_We_Deliver_To_You_Espot" class="siteCatalystTag">NZ_Do_We_Deliver_To_You_Espot</div>

	<h2>New customer?</h2>
    <p>Enter your postcode to check we deliver in your area.</p>
    
    
      <div id="PostCodeMessageArea" class="errorMessage" style="display:none;">
      </div>
    
	<form name="CheckPostCode" method="post" action="/webapp/wcs/stores/servlet/CheckPostCode" id="Rhs_checkPostCode">
		<input type="hidden" name="langId" value="44"/>
		<input type="hidden" name="storeId" value="10151"/>
		<input type="hidden" name="currentPageUrl" value="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet//ProductDisplay?catalogId=10122&amp;level=2&amp;errorViewName=ProductDisplayErrorView&amp;langId=44&amp;categoryId=185749&amp;productInRange=true&amp;productId=149116&amp;storeId=10151"/>
         
            <input type="hidden" name="currentViewName" value="ProductDisplayView"/>
        
		<input type="hidden" name="messageAreaId" value="PostCodeMessageArea"/>
		
		<div class="field">
			<div class="indicator">
				<label class="access" for="postCode">Postcode</label>
			</div>
			<div class="input">
				<input type="text" name="postCode" id="postCode" maxlength="8" value="" />
			</div>
		</div>
		<div class="actions">
			<input class="button primary process" type="submit" value="Check postcode"/>
		</div>      
	</form>
</div>
<!-- END RightHandSide.jsp --><!-- BEGIN MiniShopCartDisplay.jsp --><!-- If we get here from a generic error this service will fail so we need to catch the exception -->
		<div class="panel infoPanel">
			<span class="icon infoIcon"></span>
		   	<h2>Important Information</h2>
			<p>Alcohol promotions available to online customers serviced from our Scottish stores may differ from those shown when browsing our site. Please log in to see the full range of promotions available to you.</p>
		</div>
	<!-- END MiniShopCartDisplay.jsp -->
</div>
<!-- END RightHandSide.jspf -->
      </div>
      <!-- auxiliary End -->
    </div>
    <!-- Main Area End --><!-- Footer Start Start --><!-- BEGIN LayoutContainerBottom.jspf --><!-- BEGIN FooterDisplay.jspf -->


<div id="globalFooter" class="footer">
    <ul>
	<li><a href="http://www.sainsburys.co.uk/privacy">Privacy policy</a></li>
	<li><a href="http://www.sainsburys.co.uk/cookies">Cookie policy</a></li>
	<li><a href="http://www.sainsburys.co.uk/terms">Terms &amp; conditions</a></li>
	<li><a href="http://www.sainsburys.co.uk/accessibility">Accessibility</a></li>
	<li><a href="http://help.sainsburys.co.uk/" rel="external" target="_blank" title="Opens in new window">Help Centre</a></li>
	<li><a href="http://www.sainsburys.co.uk/getintouch">Contact us</a></li>
	<li><a href="/webapp/wcs/stores/servlet/DeviceOverride?deviceId=-21&langId=44&storeId=10151">Mobile</a></li>
</ul>

</div>

<!-- END FooterDisplay.jspf --><!-- END LayoutContainerBottom.jspf --><!-- Footer Start End -->
        
  </div><!--// End #page  --><!-- Bright Tagger start -->

	<div id="sitecatalyst_ws" class="siteCatalystTag"></div>
	
    <script type="text/javascript">
        var brightTagStAccount = 'sp0XdVN';
    </script>
    <noscript>
        <iframe src="//s.thebrighttag.com/iframe?c=sp0XdVN" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </noscript>
	
<!-- Bright Tagger End -->
</body>
</html>
<!-- END ProductDisplay.jsp -->
EOF;

        $obj = new \StdClass();
        $obj->title       = "Sainsbury's Apricot Ripe & Ready x5";
        $obj->unit_price  = 3.5;
        $obj->description = "Apricots";
        $obj->size        = "15.3kb";

        $this->parse($content)->shouldBeLike($obj);
    }

    /**
     *
     */
    function it_is_not_parseable()
    {
        $this->parse("")->shouldBe(null);
    }

    function it_is_not_findable()
    {
        $this->findInContent('', '/(aawdwad)/')->shouldBe(false);
    }
}
