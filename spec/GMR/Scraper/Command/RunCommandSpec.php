<?php
/**
 * RunCommandSpec.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace spec\GMR\Scraper\Command;

use GMR\Scraper\Command\RunCommand;
use GMR\Scraper\Output\JSON;
use GMR\Scraper\Parser\ParserInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RunSpec
 *
 * @package spec\GMR\Scraper\Command
 */
class RunCommandSpec extends ObjectBehavior
{

    /**
     *
     */
    public function it_is_initializable()
    {
        $this->shouldHaveType(RunCommand::class);
    }

    /**
     *
     */
    public function it_is_configurable()
    {
        $this->shouldNotThrow();

        $this->configure();
    }

    /**
     *
     */
    public function it_is_executeable()
    {
        $input = new ArrayInput(['run']);
        $output = new BufferedOutput();
        $this->execute($input, $output)->shouldReturn(null);
    }

    public function it_is_able_to_fetch()
    {
        $this->shouldNotThrow();

        $this->getContent(dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'Fixtures' . DIRECTORY_SEPARATOR . 'blank')->shouldReturn('');
    }

    public function it_is_able_to_parse(ParserInterface $parser)
    {
        $this->shouldNotThrow();

        $this->parse('', $parser);
    }

    public function it_is_able_to_output(JSON $formatter)
    {
        $this->shouldNotThrow();

        $results = [];

        $this->output($results, $formatter);
    }

}
