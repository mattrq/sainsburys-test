<?php
/**
 * JSONSpec.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace spec\GMR\Scraper\Output;

use GMR\Scraper\Output\JSON;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Output\BufferedOutput;

class JSONSpec extends ObjectBehavior
{
    function it_is_initializable(BufferedOutput $output)
    {
        $this->beConstructedWith($output);

        $this->shouldHaveType(JSON::class);

        $this->shouldImplement(\GMR\Scraper\Output\OutputInterface::class);
    }



    function it_can_format_output(BufferedOutput $output)
    {
        $this->beConstructedWith($output);

        $this->output([(object)[
            "title" => "Sainsbury's Apricot Ripe & Ready x5",
            "unit_price" => 3.5,
            "description" => "Apricots",
            "size" => "38.3kb"
        ]]);

        $expected = <<<EOF
{
    "results": [
        {
            "title": "Sainsbury's Apricot Ripe & Ready x5",
            "unit_price": 3.5,
            "description": "Apricots",
            "size": "38.3kb"
        }
    ],
    "total": 3.5
}
EOF;


        $output->writeln($expected)->shouldBeCalled();


    }
}
