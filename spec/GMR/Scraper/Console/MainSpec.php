<?php


/**
 * MainSpec.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace spec\GMR\Scraper\Console;

use GMR\Scraper\Console\Main;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Output\BufferedOutput;

class MainSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Main::class);

        $this->shouldBeAnInstanceOf(\Symfony\Component\Console\Application::class);
    }

    function it_is_runable()
    {
        $input = new ArrayInput(['help']);
        $output = new BufferedOutput();
        $this->doRun($input, $output)->shouldReturn(0);
    }
}
