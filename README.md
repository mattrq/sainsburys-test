# Sainsbury's - Test Scrape command #

MVP command to meet the requirements of docs/

## Summary ##

Thank you for the time taken to review this code.
The code and functionality has been kept simple, there are 
TODO markers in the code where I believe the code could be
adjusted to be more flexible, should the requirements change.
 
The main command is based on the symfony/command structure.
It requires very little of the symfony libraries but adds a
clean hook for running console commands. 


## Prerequisites ##
* php >5.4
* composer

## Install and build ##
* Clone from GIT
* `composer install`

Or if only running the application

* `composer install --no-dev`

## Run Tests ##
* `bin/phpspec run`
* `bin/phpcpd  src/`
* `bin/phpmd  src/  text cleancode,codesize,naming`
* `bin/phpcs --standard=PSR2 src/`

Or run `./build.sh`

## Run Applications ##
* `bin/scrape run`


### PHP Docs ###
This has been disabled due to the number of libraries it pulls in.

Run the following to add the required libraries "require-dev"
`composer require --dev phpdocumentor/phpdocumentor=v2.9.0`

The following command can then be run:

`bin/phpdoc -d src/ -f bin/scrape -t ./docs/phpdoc`
