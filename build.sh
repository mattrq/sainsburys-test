#!/bin/bash

composer install
#bin/phpdoc -d src/ -f bin/scrape -t ./docs/phpdoc # Disabled, see readme

php -v | grep "PHP 7" > /dev/null 2>&1
if [ $? -eq 0 ]; then
    phpdbg -qrr bin/phpspec run --no-code-generation -f pretty
else
    bin/phpspec run --no-code-generation -f pretty
fi

bin/phpcpd src/
bin/phpmd src/ text cleancode,codesize,naming
bin/phpcs --standard=PSR2 src/
