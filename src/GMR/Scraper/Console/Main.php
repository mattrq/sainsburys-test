<?php


/**
 * Main.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace GMR\Scraper\Console;

use GMR\Scraper\Command\RunCommand;
use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * The command line application entry point
 *
 * @internal
 */
class Main extends BaseApplication
{
    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    public function doRun(InputInterface $input, OutputInterface $output)
    {
        $this->add(new RunCommand());

        return parent::doRun($input, $output);
    }
}
