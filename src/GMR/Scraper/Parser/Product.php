<?php
/**
 * Procduct.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:27
 */

namespace GMR\Scraper\Parser;

/**
 * Class Product
 *
 * @package GMR\Scraper\Parser
 */
class Product implements ParserInterface
{

    /**
     * @param string $content
     *
     * @return null|\StdClass
     */
    public function parse($content)
    {
        $patterns = [
            'title' => '/<h1>(.*)<\/h1>/',
            'unit_price' => '/' .  preg_quote('<p class="pricePerUnit">', '/') . "\n" . preg_quote('&pound;', '/') .
                '([0-9]+(\.[0-9]+)?)</',
            'description' => '/' . preg_quote('<h3 class="productDataItemHeader">Description</h3>', '/') . "\n" .
                preg_quote('<div class="productText">', '/') . '(.*)' . preg_quote('</div>', '/') . '/Ums'
        ];

        $obj = new \StdClass();
        foreach ($patterns as $field => $pattern) {
            $value = $this->findInContent($content, $pattern);

            if (false === $value) {
                return null;
            }

            $obj->{$field} = $value;
        }

        $obj->unit_price = doubleval($obj->unit_price);

        $obj->size = number_format(strlen($content) / 1024, 1) . 'kb';

        return $obj;
    }

    /**
     * @param $content
     * @param $pattern
     *
     * @return bool|string
     */
    public function findInContent($content, $pattern)
    {
        if (preg_match($pattern, $content, $match) && isset($match[1])) {
            return trim(
                html_entity_decode(
                    strip_tags(
                        $match[1]
                    )
                )
            );
        }

        return false;
    }
}
