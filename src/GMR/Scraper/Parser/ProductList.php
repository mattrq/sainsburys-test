<?php
/**
 * ProductList.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:27
 */

namespace GMR\Scraper\Parser;

/**
 * Class ProductList
 *
 * @package GMR\Scraper\Parser
 */
class ProductList implements ParserInterface
{

    /**
     * @param string $content
     */
    public function parse($content)
    {
        $pattern = '/' . preg_quote('<div class="productInfo">', '/') . '.*href="(.*)"' . '/Ums';

        preg_match_all($pattern, $content, $matches);

        if (!isset($matches[1]) || empty($matches[1])) {
            return [];
        }

        return $matches[1];
    }
}
