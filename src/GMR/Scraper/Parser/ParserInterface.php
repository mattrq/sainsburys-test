<?php
/**
 * ParserInterface.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:27
 */


namespace GMR\Scraper\Parser;

/**
 * Interface ParserInterface
 * @package GMR\Scraper\Parser
 */
interface ParserInterface
{
    /**
     * @param string $content
     * @return mixed
     */
    public function parse($content);
}
