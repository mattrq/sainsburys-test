<?php
/**
 * JSON.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace GMR\Scraper\Output;

use Symfony\Component\Console\Output\OutputInterface;
use GMR\Scraper\Output\OutputInterface as OutInterface;

/**
 * Class JSON
 * @package GMR\Scraper\Output
 */
class JSON implements OutInterface
{
    /** @var  OutputInterface */
    private $output;

    /**
     * JSON constructor.
     * @param OutputInterface $output
     */
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function output(array $results)
    {
        $sum = array_sum(
            array_map(
                function ($item) {
                    return $item->unit_price;
                },
                $results
            )
        );

        $this->output->writeln(json_encode(
            [
                'results' => $results,
                'total' => $sum
            ],
            JSON_PRETTY_PRINT
        ));
    }
}
