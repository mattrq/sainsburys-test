<?php
/**
 * OutputInterface.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace GMR\Scraper\Output;

/**
 * Interface OutputInterface
 * @package GMR\Scraper\Output
 */
interface OutputInterface
{
    /**
     * @param array $results
     * @return mixed
     */
    public function output(array $results);
}
