<?php
/**
 * RunCommand.php
 * =============================================
 *
 * @copyright Matt Rosenquist 2016.
 * @author      Matt Rosenquist <mattr@gmail.com>
 * @package    GMR\Scraper
 * @version     1.0
 *
 * 19/10/16 11:25
 */

namespace GMR\Scraper\Command;

use GMR\Scraper\Output\JSON;
use GMR\Scraper\Parser\ParserInterface;
use GMR\Scraper\Parser\Product;
use GMR\Scraper\Parser\ProductList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Main command, responsible for running the specs
 *
 */
class RunCommand extends Command
{

    /**
     * Configure command
     */
    public function configure()
    {
        $this
            ->setName('run')
            ->setDefinition(array(
                new InputOption(
                    'output',
                    'o',
                    InputOption::VALUE_REQUIRED,
                    'Bootstrap php file that is run before the specs',
                    'json'
                )
            ))
            ->setDescription('Runs specifications')
            ->setHelp(<<<EOF
The <info>%command.name%</info> command runs specifications:

  <info>php %command.full_name%</info>


EOF
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return mixed
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        // TODO: Remove hardcoded reference, add to workflow
        $url = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';

        $content = $this->getContent($url);

        $productUrls = $this->parse($content, new ProductList());

        $found = [];
        foreach ($productUrls as $url) {
            $content = $this->getContent($url);
            $found[] = $this->parse($content, new Product());
        }

        $found = array_filter($found);

        $this->output($found, new JSON($output));

        return null;
    }

    /**
     * @param                 $content
     * @param ParserInterface $parser
     *
     * @return mixed
     */
    public function parse($content, ParserInterface $parser)
    {
        return $parser->parse($content);
    }

    /**
     * @param $results
     * @param $formatter
     *
     * @return mixed
     */
    public function output($results, \GMR\Scraper\Output\OutputInterface $formatter)
    {
        return $formatter->output($results);
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function getContent($url)
    {
        // TODO: With time add better error handling with curl and set some time outs....
        return file_get_contents($url);
    }
}
